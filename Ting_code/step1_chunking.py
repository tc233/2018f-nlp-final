import pandas as pd
import sys
import seaborn as sns
import matplotlib.pyplot as plt
import nltk
from tqdm import tqdm
import time


# use TweetTokenizer to tokenize irregular lyrics
word_tokenizer = nltk.TweetTokenizer(strip_handles=True, reduce_len=True)

# just treat each line as a single sentence
# sent_tokenizer = nltk.tokenize  # the standard sentence tokenizer


def extract_500_from_each_genre(table, n, genres_list):
    # initialize an empty DataFrame
    d0 = {}
    result = pd.DataFrame(data=d0)
    for genre in genres_list:
        for index, value in tqdm(table[table['genre'] == genre].sample(n=n).iterrows(), total=n):
            result = result.append(value)
    return result


def classify_genre_from_word_count(wv, wc):
    """

    :param wv: the median of word count for all genres
    :param wc: word count for a song
    :return: a genre ranking index
    """
    min_word_dist = 9999
    word_index = -1
    for i in range(len(wv)):
        if abs(wv[i]-wc) < min_word_dist:
            min_word_dist = abs(wv[i]-wc)
            word_index = i
    return word_index


def chunk_accuracy(table, wv, genre_list_desc):
    accuracy_total = 0
    accuracy_times = 0
    for index, value in table.iterrows():
        accuracy_total += 1
        i = classify_genre_from_word_count(wv, value['word_count'])
        if i == genre_list_desc.index(value['genre']):
            accuracy_times += 1
    print(f'Total accuracy rate is {100 * accuracy_times / accuracy_total}%')


def word_count_sent_count(table):
    e = {}
    result = pd.DataFrame(data=e)
    for index, value in tqdm(table.iterrows(), total=table.shape[0]):
        lyrics = word_tokenizer.tokenize(value['lyrics'])
        sentences = value['lyrics'].splitlines()
        sentences = [x for x in sentences if x != '']  # remove empty strings
        if len(sentences) < 5:  # didn't separate correctly
            print(sentences)
            sentences = nltk.tokenize.sent_tokenize(value['lyrics'])
            print('=====try to see if sentence separated correctly=====')
            print(sentences)
        value['word_count'] = len(lyrics)
        value['sent_count'] = len(sentences)
        # val['genre_index'] = genre_list.index(val['genre'])
        result = result.append(value)
    return result


if __name__ == '__main__':
    cwd = sys.path[0]  # real cwd
    T = pd.read_csv(cwd + '/../dataset/lyrics_clean_small_1500.csv')  # _small
    pd.set_option('display.expand_frame_repr', False)

    # number_of_entries_under_each_genre = 500, should be less than 700
    num = 500

    # try to see the difference between split and tokenizer
    # T['word_count'] = T['lyrics'].str.split().str.len()
    # print(T['word_count'].groupby(T['genre']).describe())
    # time.sleep(1)
    genre_list = sorted(list(set(T['genre'])))
    print('All the available genres are:', genre_list)
    time.sleep(1)

    T0 = extract_500_from_each_genre(T, num, genre_list)
    d = {}
    result_table = pd.DataFrame(data=d)

    for ind, val in tqdm(T0.iterrows(), total=T0.shape[0]):
        lyric = word_tokenizer.tokenize(val['lyrics'])
        sents = val['lyrics'].splitlines()
        sents = [x for x in sents if x != '']  # remove empty strings
        if len(sents) < 3:  # didn't separate correctly
            print(sents)
            sents = nltk.tokenize.sent_tokenize(val['lyrics'])
            print('=====try to see if sentence separated correctly=====')
            print(sents)
        val['word_count'] = len(lyric)
        val['sent_count'] = len(sents)  # some more clean up work need to be done to count the sentences
        # val['genre_index'] = genre_list.index(val['genre'])
        result_table = result_table.append(val)

    time.sleep(1)
    print('-----------')
    result_description = result_table['word_count'].groupby(
        result_table['genre']).describe()
    print(result_description)
    word_median = result_description['50%']
    print('-----------')
    result_description = result_table['sent_count'].groupby(
        result_table['genre']).describe()
    print(result_description)
    sentence_median = result_description['50%']
    print(genre_list)
    print('-----------')

    test_table = pd.read_csv(cwd + '/../dataset/lyrics_clean_small_150.csv')
    test_table = word_count_sent_count(test_table)

    sns.boxplot(x='genre', y='word_count', data=result_table)
    plt.xlabel('Genres')
    plt.ylabel('Word Count')
    plt.title(f'Word Count distribution of all {len(genre_list)} genres')
    # plt.xticks(rotation=45)
    plt.show()

    sns.boxplot(x='genre', y='sent_count', data=result_table)
    plt.show()

    chunk_accuracy(test_table, word_median, genre_list)
