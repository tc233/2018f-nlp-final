"""
a quick way to get some basic knowledge of a dataset
"""
import pandas as pd
import sys
import seaborn as sns
import matplotlib.pyplot as plt
import time

filename = 'lyrics_clean'


if __name__ == '__main__':

    cwd = sys.path[0]  # real cwd
    input_csv_table = pd.read_csv(cwd + f'/../dataset/{filename}.csv')
    # file_path = str(input('Please imput the full path or drag the file into term:')).strip('"')
    # input_csv_table = pd.read_csv(file_path)
    # for describe display
    pd.set_option('display.expand_frame_repr', False)

    print('=====0. general info=====')
    print(input_csv_table.info())

    print('=====1. genre distribution=====')
    print(input_csv_table.genre.value_counts())

    print('=====2. artists rankings=====')
    rank_artist = input_csv_table['artist'].value_counts()
    print(rank_artist.head(100))

    print('=====3. wordcount rankings=====')
    # tokenizer = nltk.TweetTokenizer(strip_handles=True, reduce_len=True)
    input_csv_table['word_count'] = input_csv_table['lyrics'].str.split().str.len()
    print(input_csv_table['word_count'].groupby(input_csv_table['genre']).describe())

    print('=====4. year rankings=====')
    print(input_csv_table['year'].groupby(input_csv_table['genre']).describe())

    sns.violinplot(x=input_csv_table['word_count'], inner='quartile')
    sns.violinplot(x=input_csv_table['word_count'], inner='box')
    plt.xlabel('Word Count')
    plt.ylabel('# of Songs')
    # plt.yticks(ticks=range(20000), labels=[i*100 for i in range(20)])
    plt.title('Word Count distribution of all songs')
    # plt.xticks(rotation=45)
    plt.show()

    sns.boxplot(x='genre', y='word_count', data=input_csv_table)
    plt.xlabel('Genres')
    plt.ylabel('Word Count')
    plt.title('Word Count distribution of all genres')
    plt.xticks(rotation=45)
    plt.show()
