"""
a quick way to peek the top 50 entries of a data set, since most tables are
too large to open in editors or excel
"""
import pandas as pd
import sys


filename = 'lyrics_clean.csv'


if __name__ == '__main__':

    cwd = sys.path[0]  # real cwd
    input_csv_table = pd.read_csv(cwd + '/../dataset/' + filename)
    pd.set_option('display.expand_frame_repr', False)

    print('=====0. general info=====')
    print(input_csv_table.info())

    print('=====1. genre distribution=====')
    print(input_csv_table.genre.value_counts())

    print('=====2. head entries=====')
    print(input_csv_table.head(50))
