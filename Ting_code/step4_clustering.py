import pandas as pd
import sys
import seaborn as sns
import matplotlib.pyplot as plt
import nltk
from tqdm import tqdm
import time
import math
from collections import defaultdict, deque, Counter
import string


# use TweetTokenizer to tokenize irregular lyrics
word_tokenizer = nltk.TweetTokenizer(strip_handles=True, reduce_len=True)


def extract_n_from_each_artist_and_entropy(table, n, artists_list, artists_dict):
    # initialize an empty DataFrame
    d0 = {}
    result = pd.DataFrame(data=d0)
    for artist in artists_list:
        print('=====current artist:', artist, '=====')
        words = []
        words1 = []
        for index, value in tqdm(table[table['artist'] == artist].sample(n=n).iterrows(), total=n):
            lyric = word_tokenizer.tokenize(value['lyrics'])
            lyric = [x.lower() for x in lyric]
            lyric_clean = [x.translate({ord(c): None for c in string.punctuation}) for x in lyric]
            lyric_clean = [x for x in lyric_clean if x != '']
            words.extend(lyric_clean)
            words1.append(lyric)  # append a list to avoid overlapping issues
            value['word_count'] = len(lyric)
            model, stats = markov_model(lyric_clean, 0)
            e_r = entropy_rate(model, stats)
            value['entropy'] = e_r
            result = result.append(value)
    return result


def markov_model(stream, model_order):
    model, stats = defaultdict(Counter), Counter()
    circular_buffer = deque(maxlen=model_order)

    for token in stream:
        prefix = tuple(circular_buffer)
        circular_buffer.append(token)
        if len(prefix) == model_order:
            stats[prefix] += 1
            model[prefix][token] += 1
    return model, stats


def markov_model_list(stream_list, model_order):
    model, stats = defaultdict(Counter), Counter()
    circular_buffer = deque(maxlen=model_order)

    for stream in stream_list:
        for token in stream:
            prefix = tuple(circular_buffer)
            circular_buffer.append(token)
            if len(prefix) == model_order:
                stats[prefix] += 1
                model[prefix][token] += 1
    return model, stats


def entropy(stats, normalization_factor):
    return -sum(proba / normalization_factor * math.log2(proba / normalization_factor) for proba in stats.values())


def entropy_rate(model, stats):
    return sum(stats[prefix] * entropy(model[prefix], stats[prefix]) for prefix in stats) / sum(stats.values())


if __name__ == '__main__':
    cwd = sys.path[0]  # real cwd
    T = pd.read_csv(cwd + '/../dataset/lyrics_clean_artists_100.csv')  # _small
    pd.set_option('display.expand_frame_repr', False)

    # number_of_entries_under_each_artist = 500
    num = 100

    artist_list = sorted(list(set(T['artist'])))
    artist_dict = dict()  # for entropy of each artist
    for g in artist_list:
        artist_dict[g] = [0, 0, 0]  # init a zero dict

    T0 = extract_n_from_each_artist_and_entropy(T, num, artist_list, artist_dict)
    print('All the available artists are:', artist_list)
    time.sleep(1)

    result_description = T0['entropy'].groupby(T0['artist']).describe()
    entropy_median = result_description['50%']

    e = {}
    result = pd.DataFrame(data=e)
    for ind, val in tqdm(T0.iterrows(), total=T0.shape[0]):
        color = ''
        if val['artist'] == 'eminem':
            color = 'k'
            plt.plot(val['entropy'], val['word_count'],
                     marker='o', color=color,
                     markersize=8)
        if val['artist'] == 'britney-spears':
            color = 'r'
            plt.plot(val['entropy'], val['word_count'],
                     marker='o', color=color,
                     markersize=8)
        if val['artist'] == 'bob-dylan':
            color = 'b'
            plt.plot(val['entropy'], val['word_count'],
                     marker='*', color=color,
                     markersize=8)

    from matplotlib.lines import Line2D
    custom_lines = [Line2D([0], [0], marker='o', color='k', markersize=8),
                    Line2D([0], [0], marker='o', color='r', markersize=8),
                    Line2D([0], [0], marker='*', color='b', markersize=8)]
    plt.legend(custom_lines, ('eminem', 'britney-spears', 'bob-dylan'))

    plt.autoscale(enable=True, tight=False)
    plt.xlabel('Entropy')
    plt.ylabel('Word Count')
    plt.title(f'Clustering of artists')

    plt.show()
