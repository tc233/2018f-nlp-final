import pandas as pd  # data processing,CSV file I/O (e.g. pd.read_csv)
import sys
from tqdm import tqdm


if __name__ == '__main__':
    cwd = sys.path[0]  # real cwd
    T = pd.read_csv(cwd + '/../dataset/lyrics_clean.csv')
    pd.set_option('display.expand_frame_repr', False)

    n = 50

    years = [1985, 2005, 2015]

    print('=====0. original lyrics dataset=====')
    print(T['genre'].value_counts())
    print('-----')

    print('=====1. original lyrics dataset=====')
    print(T['year'].value_counts())
    print('-----')

    print('=====2. general info=====')
    # initialize an empty DataFrame
    d = {}
    result_table = pd.DataFrame(data=d)

    # 1. 1965 - 1985
    for index, value in tqdm(T[T['year'] < 1985].sample(n=n).iterrows(), total=n):
        value['year'] = 1985
        result_table = result_table.append(value)

    # 2. 1985 -2005
    temp_T = T[T['year'] < 2005]
    for index, value in tqdm(temp_T[temp_T['year'] >= 1985].sample(n=n).iterrows(), total=n):
        value['year'] = 2005
        result_table = result_table.append(value)

    # 3. 2005 - now
    for index, value in tqdm(T[T['year'] >= 2005].sample(n=n).iterrows(), total=n):
        value['year'] = 2015
        result_table = result_table.append(value)

    print('=====1. genre distribution=====')
    print(result_table['year'].value_counts())
    result_table.to_csv(cwd + f'/../dataset/lyrics_clean_years_{n}.csv')

