import pandas as pd  # data processing,CSV file I/O (e.g. pd.read_csv)
import sys


if __name__ == '__main__':
    cwd = sys.path[0]  # real cwd
    T = pd.read_csv(cwd + '/../dataset/lyrics_clean.csv')
    pd.set_option('display.expand_frame_repr', False)

    n = 100

    artists = ['bob-dylan', 'drake', 'eminem', 'elvis-presley', 'britney-spears', 'celine-dion', 'the-carpenters', 'beyonce-knowles']
    # artists = ['eminem', 'elvis-presley','celine-dion']

    print('=====0. original lyrics dataset=====')
    print(T['genre'].value_counts())
    print('-----')

    print('=====1. original lyrics dataset=====')
    print(T['artist'].value_counts())
    print('-----')

    print('=====2. general info=====')
    # initialize an empty DataFrame
    d = {}
    result_table = pd.DataFrame(data=d)
    for artist in artists:
        print(artist)
        t = T[T['artist'] == artist].sample(n=n)
        result_table = result_table.append(t)

    result_table.to_csv(cwd + f'/../dataset/lyrics_clean_artists_{n}.csv')

