from nltk.sentiment.vader import SentimentIntensityAnalyzer
import pandas as pd
import sys
import nltk
from tqdm import tqdm
import time


sid = SentimentIntensityAnalyzer()


def extract_n_from_each_artist(table, n, artists_list):
    # initialize an empty DataFrame
    d0 = {}
    result = pd.DataFrame(data=d0)
    for artist in artists_list:
        # artist_sents = []
        for index, value in tqdm(table[table['artist'] == artist].sample(n=n).iterrows(), total=n):

            sents = value['lyrics'].splitlines()
            sents = [x for x in sents if x != '']  # remove empty strings
            if len(sents) < 3:  # didn't separate correctly
                # print(sents)
                sents = nltk.tokenize.sent_tokenize(value['lyrics'])
                print('=====try to see if lyric separated correctly=====')
                # print(sents)

            # artist_sents.extend(nltk.tokenize.sent_tokenize(value['lyrics']))
            # artist_sents.extend(sents)
            avg_sentiment = 0
            neg_count = 0
            neu_count = 0
            pos_count = 0
            for sentence in sents:
                ss = sid.polarity_scores(sentence)
                avg_sentiment += ss['compound']
                if ss['neg'] > 0:
                    neg_count += 1
                if ss['neu'] > 0:
                    neu_count += 1
                if ss['pos'] > 0:
                    pos_count += 1
            value['avg_sentiment'] = avg_sentiment / len(sents)
            # {'neg': 0.0, 'neu': 0.263, 'pos': 0.737, 'compound': 0.4215}
            value['sentiment_dist'] = [neg_count, neu_count, pos_count]
            # print(value['avg_sentiment'], value['sentiment_dist'])
            value['neg'] = neg_count
            value['neu'] = neu_count
            value['pos'] = pos_count
            result = result.append(value)

        # for sentence in artist_sents:
        #     print('=============')
        #     print(sentence)
        #     ss = sid.polarity_scores(sentence)
        #     for k in sorted(ss):
        #         # compound: 0.0, neg: 0.0, neu: 1.0, pos: 0.0
        #         print('{0}: {1}, '.format(k, ss[k]), end='')
        #     print()

    return result


def set_sentiment(table):
    d0 = {}
    result = pd.DataFrame(data=d0)
    for index, value in tqdm(table.iterrows(), total=table.shape[0]):
        sents = value['lyrics'].splitlines()
        sents = [x for x in sents if x != '']  # remove empty strings
        if len(sents) < 5:  # didn't separate correctly
            # print(sents)
            sents = nltk.tokenize.sent_tokenize(value['lyrics'])
            print('=====try to see if lyric separated correctly=====')
            # print(sents)
        avg_sentiment = 0
        neg_count = 0
        neu_count = 0
        pos_count = 0
        for sentence in sents:
            ss = sid.polarity_scores(sentence)
            avg_sentiment += ss['compound']
            if ss['neg'] > 0:
                neg_count += 1
            if ss['neu'] > 0:
                neu_count += 1
            if ss['pos'] > 0:
                pos_count += 1
        value['avg_sentiment'] = avg_sentiment / len(sents)
        # {'neg': 0.0, 'neu': 0.263, 'pos': 0.737, 'compound': 0.4215}
        value['sentiment_dist'] = [neg_count, neu_count, pos_count]
        value['neg'] = neg_count
        value['neu'] = neu_count
        value['pos'] = pos_count
        # print(value['avg_sentiment'], value['sentiment_dist'])
        result = result.append(value)
    return result


def classify_artist_from_sentiment_single(v1, v2, diff):
    temp_diff = 0
    if len(v1) != len(v2):
        print('Internal Error! Abort.')
        exit()
    for i in range(len(v1)):
        temp_diff += abs(v1[i]-v2[i])
    if temp_diff < diff:
        return True, temp_diff
    else:
        return False, diff


def classify_artist_from_sentiment_median(wv, wc):
    min_word_dist = 9999
    word_index = -1
    for i in range(len(wv)):
        if abs(wv[i] - wc) < min_word_dist:
            min_word_dist = abs(wv[i] - wc)
            word_index = i
    return word_index


def sentiment_accuracy(test_table, median, artist_list_desc, train_table):
    accuracy_total = 0
    accuracy_times = 0
    for index, value in test_table.iterrows():
        accuracy_total += 1
        i = classify_artist_from_sentiment_median(median, value['avg_sentiment'])
        if i == artist_list_desc.index(value['artist']):
            accuracy_times += 1
    print(f'median sentiment rate is {100 * accuracy_times / accuracy_total}%')

    accuracy_total = 0
    accuracy_times = 0

    # create an empty data frame
    d0 = {}
    for index2, value2 in tqdm(test_table.iterrows(), total=test_table.shape[0]):
        accuracy_total += 1
        min_diff = 9999
        v = pd.DataFrame(data=d0)
        for index1, value1 in train_table.iterrows():  # loop train set
            flag, min_diff = classify_artist_from_sentiment_single(value1['sentiment_dist'], value2['sentiment_dist'], min_diff)
            if flag:  # update the recent value
                v = value1
        if v['artist'] == value2['artist']:
            accuracy_times += 1
    print(f'single sentiment rate is {100 * accuracy_times / accuracy_total}%')


if __name__ == '__main__':
    cwd = sys.path[0]  # real cwd
    T = pd.read_csv(cwd + '/../dataset/lyrics_clean_artists_100.csv')  # _small
    pd.set_option('display.expand_frame_repr', False)

    # number_of_entries_under_each_artist = 500, should be less than 700
    num = 100

    artist_list = sorted(list(set(T['artist'])))

    T0 = extract_n_from_each_artist(T, num, artist_list)
    print('All the available artists are:', artist_list)
    time.sleep(1)

    result_description = T0['avg_sentiment'].groupby(T0['artist']).describe()
    print(result_description)
    sentiment_median = result_description['50%']

    test_t = pd.read_csv(cwd + '/../dataset/lyrics_clean_artists_10.csv')
    test_t = set_sentiment(test_t)
    sentiment_accuracy(test_t, sentiment_median, artist_list, T0)

    # Below to draw the correlation graph

    neg_ratio = T0['neg'].groupby(T0['artist']).describe()
    print(neg_ratio)
    neg_ratio = neg_ratio['50%'] / num

    neu_ratio = T0['neu'].groupby(T0['artist']).describe()
    print(neu_ratio)
    neu_ratio = neu_ratio['50%'] / num

    pos_ratio = T0['pos'].groupby(T0['artist']).describe()
    print(pos_ratio)
    pos_ratio = pos_ratio['50%'] / num

    com_ratio = T0['avg_sentiment'].groupby(T0['artist']).describe()
    print(com_ratio)
    com_ratio = com_ratio['50%']

    correlations = [neg_ratio, pos_ratio, com_ratio]

    import matplotlib.pyplot as plt

    plt.imshow(correlations)

    # Loop over data dimensions and create text annotations.
    float_formatter = lambda x: "%.2f" % x
    for i in range(3):
        for j in range(len(artist_list)):
            text = plt.text(j, i, float_formatter(correlations[i][j]), ha='center', va='center', color='w')

    plt.xlabel('Artists')
    plt.ylabel('Sentiments')
    plt.title('Avg. sentiment for artists')
    plt.xticks(ticks=range(len(artist_list)), labels=[i for i in artist_list])
    plt.xticks(rotation=45)
    plt.yticks(ticks=range(3), labels=['neg', 'pos', 'overall'])
    plt.colorbar()
    plt.show()
