import pandas as pd
import sys
import seaborn as sns
import matplotlib.pyplot as plt
import nltk
from tqdm import tqdm
import time
import math
from collections import defaultdict, deque, Counter
import string


# use TweetTokenizer to tokenize irregular lyrics
word_tokenizer = nltk.TweetTokenizer(strip_handles=True, reduce_len=True)


def extract_n_from_each_genre_and_entropy(table, n, genres_list, genres_dict):
    # initialize an empty DataFrame
    d0 = {}
    result = pd.DataFrame(data=d0)
    for genre in genres_list:
        print('=====current genre:', genre, '=====')
        words = []
        words1 = []
        for index, value in tqdm(table[table['genre'] == genre].sample(n=n).iterrows(), total=n):
            lyric = word_tokenizer.tokenize(value['lyrics'])
            lyric = [x.lower() for x in lyric]
            lyric_clean = [x.translate({ord(c): None for c in string.punctuation}) for x in lyric]
            lyric_clean = [x for x in lyric_clean if x != '']
            words.extend(lyric_clean)
            words1.append(lyric)  # append a list to avoid overlapping issues
            value['word_count'] = len(lyric)
            model, stats = markov_model(lyric_clean, 0)
            e_r = entropy_rate(model, stats)
            value['entropy'] = e_r
            result = result.append(value)

        model0, stats0 = markov_model(words, 0)
        e_r0 = entropy_rate(model0, stats0)
        # print(words)
        model1, stats1 = markov_model_list(words1, 1)
        e_r1 = entropy_rate(model1, stats1)

        model2, stats2 = markov_model_list(words1, 2)
        e_r2 = entropy_rate(model2, stats2)
        model3, stats3 = markov_model(words, 3)
        # model3, stats3 = markov_model_list(words1, 3)
        print(f'10 most frequent 3gram for {genre}: {stats3.most_common(10)}')
        genres_dict[genre] = [e_r0, e_r1, e_r2]
    return result


def markov_model(stream, model_order):
    model, stats = defaultdict(Counter), Counter()
    circular_buffer = deque(maxlen=model_order)

    for token in stream:
        prefix = tuple(circular_buffer)
        circular_buffer.append(token)
        if len(prefix) == model_order:
            stats[prefix] += 1
            model[prefix][token] += 1
    return model, stats


def markov_model_list(stream_list, model_order):
    model, stats = defaultdict(Counter), Counter()
    circular_buffer = deque(maxlen=model_order)

    for stream in stream_list:
        for token in stream:
            prefix = tuple(circular_buffer)
            circular_buffer.append(token)
            if len(prefix) == model_order:
                stats[prefix] += 1
                model[prefix][token] += 1
    return model, stats


def entropy(stats, normalization_factor):
    return -sum(proba / normalization_factor * math.log2(proba / normalization_factor) for proba in stats.values())


def entropy_rate(model, stats):
    return sum(stats[prefix] * entropy(model[prefix], stats[prefix]) for prefix in stats) / sum(stats.values())


def set_entropy(table):
    e = {}
    result = pd.DataFrame(data=e)
    for index, value in tqdm(table.iterrows(), total=table.shape[0]):
        lyrics = word_tokenizer.tokenize(value['lyrics'])
        lyrics = [x.lower() for x in lyrics]
        lyric_clean = [x.translate({ord(c): None for c in string.punctuation})
                       for x in lyrics]
        lyric_clean = [x for x in lyric_clean if x != '']
        model, stats = markov_model(lyric_clean, 0)
        e_r = entropy_rate(model, stats)
        value['entropy'] = e_r
        result = result.append(value)
    return result


def classify_genre_from_entropy(wv, wc):
    """

    :param wv: the median of entropy for all genres
    :param wc: entropy for a song
    :return: a genre ranking index
    """
    min_word_dist = 9999
    word_index = -1
    for i in range(len(wv)):
        if abs(wv[i]-wc) < min_word_dist:
            min_word_dist = abs(wv[i]-wc)
            word_index = i
    return word_index


def entropy_accuracy(table, median, genre_list_desc):
    accuracy_total = 0
    accuracy_times = 0
    for index, value in table.iterrows():
        accuracy_total += 1
        i = classify_genre_from_entropy(median, value['entropy'])
        if i == genre_list_desc.index(value['genre']):
            accuracy_times += 1
    print(f'Total accuracy rate is {100 * accuracy_times / accuracy_total}%')


if __name__ == '__main__':
    cwd = sys.path[0]  # real cwd
    T = pd.read_csv(cwd + '/../dataset/lyrics_clean_small_1500.csv')  # _small
    pd.set_option('display.expand_frame_repr', False)

    # number_of_entries_under_each_genre = 500, should be less than 700
    num = 500

    genre_list = sorted(list(set(T['genre'])))
    genre_dict = dict()  # for entropy of each genre
    for g in genre_list:
        genre_dict[g] = [0, 0, 0]  # init a zero dict

    T0 = extract_n_from_each_genre_and_entropy(T, num, genre_list, genre_dict)
    print('All the available genres are:', genre_list)
    time.sleep(1)

    result_description = T0['entropy'].groupby(T0['genre']).describe()
    print(result_description)
    entropy_median = result_description['50%']

    test_table = pd.read_csv(cwd + '/../dataset/lyrics_clean_small_150.csv')
    test_table = set_entropy(test_table)

    # 0, 1, 2nd order entropy rate
    # for g in genre_dict:
    #     print(g, genre_dict[g])

    sns.boxplot(x='genre', y='entropy', data=T0)
    plt.xlabel('Genres')
    plt.ylabel('Entropy')
    plt.title(f'Entropy distribution of all {len(genre_list)} genres')
    plt.show()

    entropy_accuracy(test_table, entropy_median, genre_list)

