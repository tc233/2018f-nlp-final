"""
Select the top ranking(# of songs) artists from each genre
and generate a sub table
"""
import pandas as pd  # data processing,CSV file I/O (e.g. pd.read_csv)
import sys


if __name__ == '__main__':
    cwd = sys.path[0]  # real cwd
    T = pd.read_csv(cwd + '/../dataset/lyrics_clean.csv')
    pd.set_option('display.expand_frame_repr', False)

    print('=====0. original lyrics dataset=====')
    print(T.genre.value_counts())
    print('-----')

    print('=====2. general info=====')
    # initialize an empty DataFrame
    d = {}
    result_table = pd.DataFrame(data=d)
    genre_set = set(T['genre'])
    for genre in genre_set:
        rank_artist = T[T['genre'] == genre]['artist'].value_counts()
        rank_artist.head(50).to_csv(cwd + f'/../dataset/rankings/lyrics_clean_artist_ranking_{genre}.csv')


