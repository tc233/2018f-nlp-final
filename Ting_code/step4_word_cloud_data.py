import pandas as pd
import sys
import nltk
from nltk.corpus import stopwords
import string
from sklearn.feature_extraction.stop_words import ENGLISH_STOP_WORDS


word_tokenizer = nltk.TweetTokenizer(strip_handles=True, reduce_len=True)


if __name__ == '__main__':
    interested_artist = 'elvis-presley'

    cwd = sys.path[0]  # real cwd
    T = pd.read_csv(cwd + '/../dataset/lyrics_clean_artists_100.csv')  # _small
    pd.set_option('display.expand_frame_repr', False)

    artist_list = sorted(list(set(T['artist'])))

    result = []
    english_stopwords = set(stopwords.words('english')) | ENGLISH_STOP_WORDS | {
    'ya', 'aah', 'ye', 'hey', 'ba', 'da', 'buh', 'duh', 'doo', 'ooh',
    'woo', 'uh', 'hoo', 'ah', 'yeah', 'oo', 'la', 'chorus', 'beep', 'ha'}

    for ind, val in T[T['artist'] == interested_artist].iterrows():

        lyric = word_tokenizer.tokenize(val['lyrics'])
        lyric = [x.lower() for x in lyric]
        lyric_clean = [x.translate({ord(c): None for c in string.punctuation}) for x in lyric]
        lyric_clean = [x for x in lyric_clean if x != '']
        lyric_clean = [x for x in lyric_clean if (x not in english_stopwords)]
        result.extend(lyric_clean)

    result = sorted(result)

    with open(cwd + '/../dataset/wordcloud_words.txt', mode='w') as output:
        for w in result:
            output.write(w)
            output.write('\n')

