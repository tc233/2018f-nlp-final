"""
select n=1500 songs from given genres and generate a sub table for further use
"""
import pandas as pd  # data processing,CSV file I/O (e.g. pd.read_csv)
import nltk
import sys
from tqdm import tqdm

# use TweetTokenizer to tokenize irregular lyrics
tokenizer = nltk.TweetTokenizer(strip_handles=True, reduce_len=True)


# def clear_out_length_lyric(index, line, to_drop_indices_list_ref):
#     lyric = tokenizer.tokenize(line['lyrics'])
#     if len(lyric) > 500:  # a lyric with more than 800 words...
#         to_drop_indices_list_ref.append(index)
#         return
#     if len(lyric) < 200:  # just make sure shorter ones won't affect result
#         to_drop_indices_list_ref.append(index)
#         return


if __name__ == '__main__':
    cwd = sys.path[0]  # real cwd
    T = pd.read_csv(cwd + '/../dataset/lyrics_clean.csv')
    pd.set_option('display.expand_frame_repr', False)

    # number_of_entries_under_each_genre = 1500
    n = 1500
    to_drop_indices_list = []

    print('=====0. original lyrics dataset=====')
    print(T.genre.value_counts())
    print('-----')

    # print('=====1. remove non-standardized lyrics=====')
    # for ind, val in T.iterrows():
    #     clear_out_length_lyric(ind, val, to_drop_indices_list)
    # T = T.drop(set(to_drop_indices_list))
    # to_drop_indices_list = []

    print('=====2. general info=====')
    # initialize an empty DataFrame
    d = {}
    result_table = pd.DataFrame(data=d)
    # genre_set = set(T['genre'])
    genres = ['Rock', 'Hip-Hop', 'Electronic', 'Jazz']
    for genre in genres:
        # for ind, val in tqdm(T[T['genre'] == genre].sample(n=n).iterrows(), total=n):
        for ind, val in tqdm(T[T['genre'] == genre][:n].iterrows(),
                             total=n):
            result_table = result_table.append(val)
    print(result_table.info())
    print(result_table['year'].groupby(result_table['genre']).describe())
    print(result_table['genre'].value_counts())

    result_table.to_csv(cwd + f'/../dataset/lyrics_clean_small_{n}.csv')

