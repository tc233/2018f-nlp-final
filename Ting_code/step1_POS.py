from nltk.sentiment.vader import SentimentIntensityAnalyzer
import pandas as pd
import sys
import seaborn as sns
import matplotlib.pyplot as plt
import nltk
from tqdm import tqdm
import time
import math
from collections import defaultdict, deque, Counter
import string


# use TweetTokenizer to tokenize irregular lyrics
word_tokenizer = nltk.TweetTokenizer(strip_handles=True, reduce_len=True)


def extract_n_from_each_genre_pos(table, n, genres_list, genres_dict):
    # initialize an empty DataFrame
    d0 = {}
    result = pd.DataFrame(data=d0)
    for genre in genres_list:
        genre_stats = Counter()
        for index, value in tqdm(table[table['genre'] == genre].sample(n=n).iterrows(), total=n):
            all_stats = Counter()
            sentences = value['lyrics'].splitlines()
            sentences = [x for x in sentences if x != '']  # remove empty strings
            if len(sentences) < 5:  # didn't separate correctly
                sentences = nltk.tokenize.sent_tokenize(value['lyrics'])
            for sentence in sentences:
                lyric = word_tokenizer.tokenize(sentence)
                wrap = nltk.pos_tag(lyric, tagset='universal')
                # print(wrap)
                result_set, result_tags = zip(*wrap)
                stats = Counter(result_tags)
                # print(stats)
                all_stats = all_stats + stats
            # for each lyric, record it tags
            value['tag_counter'] = all_stats
            result = result.append(value)
            genre_stats += all_stats
        genres_dict[genre] = genre_stats
        print(f'{genre} stats are:', genre_stats)
    return result


def set_pos(table):
    e = {}
    result = pd.DataFrame(data=e)
    for index, value in tqdm(table.iterrows(), total=table.shape[0]):
        all_stats = Counter()
        sentences = value['lyrics'].splitlines()
        sentences = [x for x in sentences if x != '']  # remove empty strings
        if len(sentences) < 5:  # didn't separate correctly
            sentences = nltk.tokenize.sent_tokenize(value['lyrics'])
        for sentence in sentences:
            lyric = word_tokenizer.tokenize(sentence)
            wrap = nltk.pos_tag(lyric, tagset='universal')
            result_set, result_tags = zip(*wrap)
            stats = Counter(result_tags)
            all_stats = all_stats + stats
        # for each lyric, record it tags
        value['tag_counter'] = all_stats
        result = result.append(value)
    return result


def counter_to_ratio_count(c):
    t_l = []
    l2 = len(sorted(c.elements()))
    # calculate the rate of tags
    for i in c:
        for j in range(int(c[i] / l2 * 10000)):
            t_l.append(i)
    return Counter(t_l)


def abs_subtract(c1, c2):
    c1.subtract(c2)
    t_l = []
    for i in c1:
        for j in range(abs(c1[i])):
            t_l.append(i)
    return Counter(t_l)


def classify_genre_from_all_pos(d, v):
    test_counter_0 = counter_to_ratio_count(v)
    # print('test', test_counter_0)
    s = 99999
    i = 0
    index = -1
    for g in d:
        test_counter = test_counter_0.copy()  # need to replicate a copy
        temp_counter = counter_to_ratio_count(d[g])
        diff_counter = abs_subtract(test_counter, temp_counter)
        # print('genre', g)
        # print('train', temp_counter)
        # print('diff', diff_counter)
        if sum(diff_counter.values()) < s:  # if less diff, record index
            s = sum(diff_counter.values())
            # print(s)
            index = i
        i += 1
    return index


def classify_genre_from_single_pos(v1, v2, diff):
    temp_v1 = counter_to_ratio_count(v1)
    temp_v2 = counter_to_ratio_count(v2)
    diff_counter = abs_subtract(temp_v1, temp_v2)
    temp_diff = sum(diff_counter.values())
    if temp_diff < diff:
        return True, temp_diff
    else:
        return False, diff


def pos_accuracy(test_table, genres_dict, genre_list_desc, train_table):
    accuracy_total = 0
    accuracy_times = 0
    for index, value in test_table.iterrows():
        accuracy_total += 1
        i = classify_genre_from_all_pos(genres_dict, value['tag_counter'])
        if i == genre_list_desc.index(value['genre']):
            accuracy_times += 1
    print(f'average PoS rate is {100 * accuracy_times / accuracy_total}%')

    accuracy_total = 0
    accuracy_times = 0

    # create an empty data frame
    d0 = {}
    for index2, value2 in tqdm(test_table.iterrows(), total=test_table.shape[0]):
        accuracy_total += 1
        min_diff = 99999
        v = pd.DataFrame(data=d0)
        for index1, value1 in train_table.iterrows():  # loop train set
            flag, min_diff = classify_genre_from_single_pos(value1['tag_counter'], value2['tag_counter'], min_diff)
            if flag:  # update the recent value
                v = value1
        if v['genre'] == value2['genre']:
            accuracy_times += 1
    print(f'single PoS rate is {100 * accuracy_times / accuracy_total}%')


if __name__ == '__main__':
    cwd = sys.path[0]  # real cwd
    T = pd.read_csv(cwd + '/../dataset/lyrics_clean_small_1500.csv')  # _small
    pd.set_option('display.expand_frame_repr', False)

    # number_of_entries_under_each_genre = 500, should be less than 700
    num = 500

    genre_list = sorted(list(set(T['genre'])))
    genre_counter_dict = dict()
    for g in genre_list:
        genre_counter_dict[g] = Counter()  # init a None, add later

    T0 = extract_n_from_each_genre_pos(T, num, genre_list, genre_counter_dict)
    print('All the available genres are:', genre_list)
    time.sleep(1)

    test_t = pd.read_csv(cwd + '/../dataset/lyrics_clean_small_15.csv')
    test_t = set_pos(test_t)
    pos_accuracy(test_t, genre_counter_dict, genre_list, T0)
