from nltk.sentiment.vader import SentimentIntensityAnalyzer
import pandas as pd
import sys
import seaborn as sns
import matplotlib.pyplot as plt
import nltk
from tqdm import tqdm
import time
import math
from collections import defaultdict, deque, Counter
import string


# use TweetTokenizer to tokenize irregular lyrics
word_tokenizer = nltk.TweetTokenizer(strip_handles=True, reduce_len=True)


def extract_n_from_each_year_pos(table, n, years_list, years_dict):
    # initialize an empty DataFrame
    d0 = {}
    result = pd.DataFrame(data=d0)
    for year in years_list:
        year_stats = Counter()
        for index, value in tqdm(table[table['year'] == year].sample(n=n).iterrows(), total=n):
            all_stats = Counter()
            sentences = value['lyrics'].splitlines()
            sentences = [x for x in sentences if x != '']  # remove empty strings
            if len(sentences) < 5:  # didn't separate correctly
                sentences = nltk.tokenize.sent_tokenize(value['lyrics'])
            for sentence in sentences:
                lyric = word_tokenizer.tokenize(sentence)
                wrap = nltk.pos_tag(lyric, tagset='universal')
                # print(wrap)
                result_set, result_tags = zip(*wrap)
                stats = Counter(result_tags)
                # print(stats)
                all_stats = all_stats + stats
            # for each lyric, record it tags
            value['tag_counter'] = all_stats
            result = result.append(value)
            year_stats += all_stats
        years_dict[year] = year_stats
        print(f'{year} stats are:', year_stats)
    return result


def set_pos(table):
    e = {}
    result = pd.DataFrame(data=e)
    for index, value in tqdm(table.iterrows(), total=table.shape[0]):
        all_stats = Counter()
        sentences = value['lyrics'].splitlines()
        sentences = [x for x in sentences if x != '']  # remove empty strings
        if len(sentences) < 5:  # didn't separate correctly
            sentences = nltk.tokenize.sent_tokenize(value['lyrics'])
        for sentence in sentences:
            lyric = word_tokenizer.tokenize(sentence)
            wrap = nltk.pos_tag(lyric, tagset='universal')
            result_set, result_tags = zip(*wrap)
            stats = Counter(result_tags)
            all_stats = all_stats + stats
        # for each lyric, record it tags
        value['tag_counter'] = all_stats
        result = result.append(value)
    return result


def counter_to_ratio_count(c):
    t_l = []
    l2 = len(sorted(c.elements()))
    # calculate the rate of tags
    for i in c:
        for j in range(int(c[i] / l2 * 10000)):
            t_l.append(i)
    return Counter(t_l)


def abs_subtract(c1, c2):
    c1.subtract(c2)
    t_l = []
    for i in c1:
        for j in range(abs(c1[i])):
            t_l.append(i)
    return Counter(t_l)


def classify_year_from_all_pos(d, v):
    test_counter_0 = counter_to_ratio_count(v)
    # print('test', test_counter_0)
    s = 99999
    i = 0
    index = -1
    for g in d:
        test_counter = test_counter_0.copy()  # need to replicate a copy
        temp_counter = counter_to_ratio_count(d[g])
        diff_counter = abs_subtract(test_counter, temp_counter)
        # print('year', g)
        # print('train', temp_counter)
        # print('diff', diff_counter)
        if sum(diff_counter.values()) < s:  # if less diff, record index
            s = sum(diff_counter.values())
            # print(s)
            index = i
        i += 1
    return index


def classify_year_from_single_pos(v1, v2, diff):
    temp_v1 = counter_to_ratio_count(v1)
    temp_v2 = counter_to_ratio_count(v2)
    diff_counter = abs_subtract(temp_v1, temp_v2)
    temp_diff = sum(diff_counter.values())
    if temp_diff < diff:
        return True, temp_diff
    else:
        return False, diff


def pos_accuracy(test_table, years_dict, year_list_desc, train_table):
    accuracy_total = 0
    accuracy_times = 0
    for index, value in test_table.iterrows():
        accuracy_total += 1
        i = classify_year_from_all_pos(years_dict, value['tag_counter'])
        if i == year_list_desc.index(value['year']):
            accuracy_times += 1
    print(f'year pos rate is {100 * accuracy_times / accuracy_total}%')

    accuracy_total = 0
    accuracy_times = 0

    # create an empty data frame
    d0 = {}
    for index2, value2 in tqdm(test_table.iterrows(), total=test_table.shape[0]):
        accuracy_total += 1
        min_diff = 99999
        v = pd.DataFrame(data=d0)
        for index1, value1 in train_table.iterrows():  # loop train set
            flag, min_diff = classify_year_from_single_pos(value1['tag_counter'], value2['tag_counter'], min_diff)
            if flag:  # update the recent value
                v = value1
        if v['year'] == value2['year']:
            accuracy_times += 1
    print(f'single pos rate is {100 * accuracy_times / accuracy_total}%')


if __name__ == '__main__':
    cwd = sys.path[0]  # real cwd
    T = pd.read_csv(cwd + '/../dataset/lyrics_clean_years_500.csv')  # _small
    pd.set_option('display.expand_frame_repr', False)

    # number_of_entries_under_each_year = 500, should be less than 700
    num = 100

    year_list = sorted(list(set(T['year'])))
    year_counter_dict = dict()
    for g in year_list:
        year_counter_dict[g] = Counter()  # init a None, add later

    T0 = extract_n_from_each_year_pos(T, num, year_list, year_counter_dict)
    print('All the available years are:', year_list)
    time.sleep(1)

    test_t = pd.read_csv(cwd + '/../dataset/lyrics_clean_years_50.csv')
    test_t = set_pos(test_t)
    pos_accuracy(test_t, year_counter_dict, year_list, T0)
