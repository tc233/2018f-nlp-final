import pandas as pd
import sys
import seaborn as sns
import matplotlib.pyplot as plt
import nltk
from tqdm import tqdm
import time
import math
from collections import defaultdict, deque, Counter
import string


# use TweetTokenizer to tokenize irregular lyrics
word_tokenizer = nltk.TweetTokenizer(strip_handles=True, reduce_len=True)
bigram = False  # True: use bigram instead of trigram


def extract_n_from_each_genre_trigram(table, n, genres_list, genres_dict):
    # initialize an empty DataFrame
    d0 = {}
    result = pd.DataFrame(data=d0)
    for genre in genres_list:
        print('=====current genre:', genre, '=====')
        words = []
        words1 = []
        for index, value in tqdm(table[table['genre'] == genre].sample(n=n).iterrows(), total=n):
            lyric = word_tokenizer.tokenize(value['lyrics'])
            lyric = [x.lower() for x in lyric]
            lyric_clean = [x.translate({ord(c): None for c in string.punctuation}) for x in lyric]
            lyric_clean = [x for x in lyric_clean if x != '']
            words.extend(lyric_clean)
            words1.append(lyric)  # append a list to avoid overlapping issues

        # maybe even add bigram feature to evaluation later?
        model2, stats2 = markov_model_list(words1, 2)
        model3, stats3 = markov_model_list(words1, 3)

        print(f'10 most frequent 3gram for {genre}: {stats3.most_common(10)}')
        # genres_dict[genre] = stats3
        genres_dict[genre] = [stats2, stats3]


def markov_model(stream, model_order):
    model, stats = defaultdict(Counter), Counter()
    circular_buffer = deque(maxlen=model_order)

    for token in stream:
        prefix = tuple(circular_buffer)
        circular_buffer.append(token)
        if len(prefix) == model_order:
            stats[prefix] += 1
            model[prefix][token] += 1
    return model, stats


def markov_model_list(stream_list, model_order):
    model, stats = defaultdict(Counter), Counter()
    circular_buffer = deque(maxlen=model_order)

    for stream in stream_list:
        for token in stream:
            prefix = tuple(circular_buffer)
            circular_buffer.append(token)
            if len(prefix) == model_order:
                stats[prefix] += 1
                model[prefix][token] += 1
    return model, stats


def entropy(stats, normalization_factor):
    return -sum(proba / normalization_factor * math.log2(proba / normalization_factor) for proba in stats.values())


def entropy_rate(model, stats):
    return sum(stats[prefix] * entropy(model[prefix], stats[prefix]) for prefix in stats) / sum(stats.values())


def set_ngram(table):
    # e = {}
    # result = pd.DataFrame(data=e)
    ngram_list = [[] for _ in range(table.shape[0])]
    for index, value in tqdm(table.iterrows(), total=table.shape[0]):
        lyrics = word_tokenizer.tokenize(value['lyrics'])
        lyrics = [x.lower() for x in lyrics]
        lyric_clean = [x.translate({ord(c): None for c in string.punctuation})
                       for x in lyrics]
        lyric_clean = [x for x in lyric_clean if x != '']
        model2, stats2 = markov_model(lyrics, 2)
        model3, stats3 = markov_model(lyrics, 3)
        ngram_list[index] = [stats2, stats3]
        # result = result.append(value)
    return ngram_list


def classify_genre_from_ngram(test_ngram, train_ngram):
    max_remain2 = -1
    max_remain3 = -1
    word_index2 = -1
    word_index3 = -1
    # both are counters, count how many overlaps occur
    test_stat2 = test_ngram[0]  # bigram
    test_stat3 = test_ngram[1]  # trigram
    i = 0
    for g in train_ngram:
        train_stat = train_ngram[g]
        # https://docs.python.org/3.6/library/collections.html#collections.Counter
        temp_counter2 = test_stat2 & train_stat[0]
        # print(f'length of counter is {len(temp_counter2)}, counter is {temp_counter2}')
        temp_counter3 = test_stat3 & train_stat[1]
        if len(temp_counter2) > max_remain2:
            max_remain2 = len(temp_counter2)
            word_index2 = i
        if len(temp_counter3) > max_remain3:
            max_remain3 = len(temp_counter3)
            word_index3 = i
        i += 1
    # if word_index2 != word_index3:
    #     # select a better guess for this lyric
    #     rate2 = max_remain2 / len(train_ngram[genre_list[word_index2]])
    #     rate3 = max_remain3 / len(train_ngram[genre_list[word_index3]])
    #     print(f'rate2:{rate2}, rate3:{rate3}')
    #     word_index = word_index3 if rate3 > rate2 else word_index2
    #     print('We choose larger one as the better guess here')
    #     return word_index
    if bigram:
        return word_index2
    return word_index3


def ngram_accuracy(table, genre_list_desc, genre_dict_ngram, l):
    accuracy_total = 0
    accuracy_times = 0
    for index, value in tqdm(table.iterrows(), total=table.shape[0]):
        accuracy_total += 1
        i = classify_genre_from_ngram(l[index], genre_dict_ngram)
        if i == genre_list_desc.index(value['genre']):
            accuracy_times += 1
    time.sleep(1)
    print(f'ngram accuracy rate is {100 * accuracy_times / accuracy_total}%')


if __name__ == '__main__':
    cwd = sys.path[0]  # real cwd
    T = pd.read_csv(cwd + '/../dataset/lyrics_clean_small_1500.csv')  # _small
    pd.set_option('display.expand_frame_repr', False)

    # number_of_entries_under_each_genre = 500, should be less than 700
    num = 500

    genre_list = sorted(list(set(T['genre'])))
    genre_dict = dict()  # for trigram set of each genre
    for g in genre_list:
        genre_dict[g] = None  # init a None, add later

    extract_n_from_each_genre_trigram(T, num, genre_list, genre_dict)

    print('All the available genres are:', genre_list)
    time.sleep(1)

    test_table = pd.read_csv(cwd + '/../dataset/lyrics_clean_small_150.csv')
    test_list = set_ngram(test_table)
    print('Start to evaluate accuracy')

    for g in genre_dict:
        print(f'10 most frequent 3gram for {g}:\n{genre_dict[g][1].most_common(10)}')

    ngram_accuracy(test_table, genre_list, genre_dict, test_list)


