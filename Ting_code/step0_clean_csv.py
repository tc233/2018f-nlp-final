"""
clean the invalid entries from original data set and generate a clean subset
"""

import pandas as pd  # data processing,CSV file I/O (e.g. pd.read_csv)
import nltk
import sys
from tqdm import tqdm
import time


# global variable
english_vocab = set(w.lower() for w in nltk.corpus.words.words())

# use TweetTokenizer to tokenize irregular lyrics
tokenizer = nltk.TweetTokenizer(strip_handles=True, reduce_len=True)


def clear_out_other_languages(index, line, to_drop_indices_list_ref):
    """
    remove lyrics in other languages

    :param index: the index in data frames in csv file
    :param line: the data frame
    :param to_drop_indices_list_ref: a list of to remove indices
    :return: nothing
    """
    try:
        lyric = tokenizer.tokenize(line['lyrics'])
    except TypeError:
        print('tokenize TypeError, delete this song:', line['song'], 'by artist:', line['artist'])
        to_drop_indices_list_ref.append(index)
        return
    lyric_vocab = set(w.lower() for w in lyric if w.lower().isalpha())
    differences = lyric_vocab.difference(english_vocab)
    ratio = len(differences) / len(lyric_vocab)  # the English word rate
    if ratio > 0.3:
        # print('lyric not English:', line['song'], 'by artist:', line['artist'], 'genre:', line['genre'], 'preview:', lyric[:10])
        to_drop_indices_list_ref.append(index)


def clear_out_other_genre(index, line, to_drop_indices_list_ref):
    if line['genre'] == 'Other' or line['genre'] == 'Not Available':
        to_drop_indices_list_ref.append(index)
    if line['year'] < 1950:  # apparently not correct
        to_drop_indices_list_ref.append(index)


def clear_out_length_lyric(index, line, to_drop_indices_list_ref):
    if pd.isna(line['lyrics']) or pd.isna(line['song']):
        to_drop_indices_list_ref.append(index)
        return
    try:
        lyric = tokenizer.tokenize(line['lyrics'])
        sentences = line['lyrics'].splitlines()
        sentences = [x for x in sentences if x != '']  # remove empty strings
        if len(sentences) < 3:  # didn't separate correctly
            sentences = nltk.tokenize.sent_tokenize(line['lyrics'])
    except TypeError:
        print('tokenize TypeError, delete this song:', line['song'], 'by artist:', line['artist'])
        to_drop_indices_list_ref.append(index)
        return
    lyric_vocab = set(w.lower() for w in lyric if w.lower().isalpha())
    if len(lyric_vocab) < 10:
        # print('Here!!! ', line['song'], 'by artist:', line['artist'], 'genre:', line['genre'], 'preview:', lyric[:10])
        # this song is just not unique in wording, just delete it
        to_drop_indices_list_ref.append(index)
        return
    if len(sentences) < 5:
        # too few sentences, still delete it...
        to_drop_indices_list_ref.append(index)
        return
    if len(lyric) < 10:  # a lyric with less than 10 words... are u serious?!
        to_drop_indices_list_ref.append(index)
        return
    if len(lyric) >= 800:  # a lyric with more than 800 words...
        to_drop_indices_list_ref.append(index)
        return
    if len(lyric) < 100:  # just make sure those short ones won't affect result
        to_drop_indices_list_ref.append(index)
        return


if __name__ == '__main__':
    cwd = sys.path[0]  # real cwd
    input_csv_table = pd.read_csv(cwd + '/../dataset/lyrics.csv')
    print(input_csv_table.genre.value_counts())
    print('-----')
    to_drop_indices_list = []

    # loop through the data set
    # for ind, val in input_csv_table.iterrows():
    #     # 1. clear out NA and Other genres
    #     clear_out_other_genre(ind, val, to_drop_indices_list)
    #     # 2. clear out not English lyrics
    #     clear_out_other_languages(ind, val, to_drop_indices_list)
    #     # 3. clear out lyrics with strange length
    #     clear_out_length_lyric(ind, val, to_drop_indices_list)

    print('debug info: here at step1')
    for ind, val in input_csv_table.iterrows():
        clear_out_other_genre(ind, val, to_drop_indices_list)

    input_csv_table = input_csv_table.drop(set(to_drop_indices_list))
    to_drop_indices_list = []

    print('debug info: here at step2')

    for ind, val in tqdm(input_csv_table.iterrows(), total=input_csv_table.shape[0]):
        clear_out_length_lyric(ind, val, to_drop_indices_list)

    input_csv_table = input_csv_table.drop(set(to_drop_indices_list))
    to_drop_indices_list = []

    print('debug info: here at step3')
    for ind, val in tqdm(input_csv_table.iterrows(), total=input_csv_table.shape[0]):
        clear_out_other_languages(ind, val, to_drop_indices_list)

    input_csv_table = input_csv_table.drop(set(to_drop_indices_list))
    to_drop_indices_list = []

    # result_table = input_csv_table.drop(set(to_drop_indices_list))
    print(input_csv_table.genre.value_counts())

    input_csv_table.to_csv(cwd + '/../dataset/lyrics_clean.csv')

