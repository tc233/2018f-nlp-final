import pandas as pd
import sys
# import nltk
import seaborn as sns
import matplotlib.pyplot as plt

from urllib.request import urlretrieve
import os
import numpy as np
import h5py
import string
import tensorflow as tf


cwd = sys.path[0]  # real cwd
df = pd.read_csv(cwd + '/../dataset/lyrics_clean_small_1500.csv')

labels = {'Rock': [1.,0.,0.,0.],
    'Hip-Hop': [0.,1.,0.,0.],
     'Jazz': [0.,0.,1.,0.],
    'Electronic': [0.,0.,0.,1.]}

tf.reset_default_graph()

vector_size = 300
sequ_len = 400

x = tf.placeholder(tf.float32, [None, sequ_len*vector_size], name='x')
sen_matrix = tf.reshape(x, [-1, sequ_len, vector_size, 1])
y = tf.placeholder(tf.float32, [None, 4], name = 'y')
"""
with tf.name_scope('conv1_core') as scope:
    W1 = tf.Variable(tf.truncated_normal([3, vector_size, 1, 100], stddev=0.1))
    b1 = tf.Variable(tf.zeros([100]))
    conv1_preact = tf.nn.conv2d(sen_matrix, W1, strides=[1, 1, 1, 1], padding='VALID') + b1
    conv1 = tf.nn.relu(conv1_preact)

with tf.name_scope('pool1_core') as scope:
    pool1 = tf.nn.max_pool(conv1,ksize=[1,398,1,1],strides=[1, 1, 1, 1],padding='VALID')

with tf.name_scope('conv2_core') as scope:
    W2 = tf.Variable(tf.truncated_normal([2, vector_size, 1, 200], stddev=0.1))
    b2 = tf.Variable(tf.zeros([200]))
    conv2_preact = tf.nn.conv2d(sen_matrix, W2, strides=[1, 1, 1, 1], padding='VALID') + b2
    conv2 = tf.nn.relu(conv2_preact)

with tf.name_scope('pool2_core') as scope:
    pool2 = tf.nn.max_pool(conv2, ksize=[1,399,1,1],strides=[1, 1, 1, 1],padding='VALID')
"""
with tf.name_scope('conv3_core') as scope:
    W3 = tf.Variable(tf.truncated_normal([1, vector_size, 1, 300], stddev=0.1))
    b3 = tf.Variable(tf.zeros([300]))
    conv3_preact = tf.nn.conv2d(sen_matrix, W3, strides=[1, 1, 1, 1], padding='VALID') + b3
    conv3 = tf.nn.relu(conv3_preact)

with tf.name_scope('pool3_core') as scope:
    pool3 = tf.nn.max_pool(conv3, ksize=[1,400,1,1],strides=[1, 1, 1, 1],padding='VALID')
"""
with tf.name_scope('pool_all_core') as scope:
    num_filters_total = 300
    pool_all = tf.concat([pool1, pool2, pool3], 3)
    pool_flat = tf.reshape(pool_all, [-1, num_filters_total])
"""
with tf.name_scope('fc_1') as scope:
    flat = tf.reshape(pool3, [-1,300])
    weights = tf.Variable(tf.truncated_normal([300, 96], dtype=tf.float32, stddev=1e-1), name='weights')
    mat = tf.matmul(flat, weights)
    biases = tf.Variable(tf.constant(0.0, shape=[96], dtype=tf.float32), trainable=True, name='biases')
    bias = tf.nn.bias_add(mat, biases)
    fc1 = tf.nn.relu(bias, name=scope)

with tf.name_scope('fc_2') as scope:
    weights = tf.Variable(tf.truncated_normal([96, 4], dtype=tf.float32, stddev=1e-1), name='weights')
    mat = tf.matmul(fc1, weights)
    biases = tf.Variable(tf.constant(0.0, shape=[4], dtype=tf.float32), trainable=True, name='biases')
    bias = tf.nn.bias_add(mat, biases)
    pred = tf.nn.softmax(bias)

# Loss and metrics
loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred, labels=y))
correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(pred, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
initialize_all = tf.global_variables_initializer()
train_step = tf.train.GradientDescentOptimizer(0.05).minimize(loss)


def convert_line(genre, lyrics):
    remove_punct=str.maketrans('','',string.punctuation)
    words = lyrics.translate(remove_punct).lower().split()

    embeddings = [normalized_embeddings[index[w]] for w in words if w in index]
    while len(embeddings) < 400:
        embeddings.append(np.zeros(300))
    embeddings = np.array(embeddings[0:400]).reshape(1, 120000)
    #x = np.mean(np.vstack(embeddings), axis=0)
    y = labels[genre]
    return {'x': embeddings[0], 'y': y}


if __name__ == '__main__':

    if not os.path.isfile('mini.h5'):
        print("Downloading Conceptnet Numberbatch word embeddings...")
        conceptnet_url = 'http://conceptnet.s3.amazonaws.com/precomputed-data/2016/numberbatch/17.06/mini.h5'
        urlretrieve(conceptnet_url, 'mini.h5')

    with h5py.File('mini.h5', 'r') as f:
        all_words = [word.decode('utf-8') for word in f['mat']['axis1'][:]]
        all_embeddings = f['mat']['block0_values'][:]
    english_words = [word[6:] for word in all_words if word.startswith('/c/en/')]
    english_word_indices = [i for i, word in enumerate(all_words) if word.startswith('/c/en/')]
    english_embedddings = all_embeddings[english_word_indices]
    norms = np.linalg.norm(english_embedddings, axis=1)
    normalized_embeddings = english_embedddings.astype('float32') / norms.astype('float32').reshape([-1, 1])
    index = {word: i for i, word in enumerate(english_words)}



    #labels_list = ['Pop', 'Rock', 'Hip-Hop', 'Metal', 'Country', 'Jazz', 'Electronic', 'R&B', 'Indie', 'Folk']
    labels_list = ['Rock', 'Hip-Hop', 'Jazz', 'Electronic']
    df_ans = df[df['genre'] == 'Rock'][0:500]
    #df_ans = df[df['genre'] == 'Hip-Hop'][0:1100]
    for curr_label in labels_list[1:]:
        curr_df = df[df['genre'] == curr_label][0:500]
        df_ans = pd.concat([df_ans, curr_df], axis=0, ignore_index=True)

    genres = df_ans['genre']
    lyrics = df_ans['lyrics']
    print(len(genres))
    #convert_line(genres[0], lyrics[0])

    dataset = [convert_line(genres[i], lyrics[i]) for i in range(len(genres))]
    print(len(dataset[0]['x']))

    import random
    random.shuffle(dataset)
    batch_size = 100
    total_batches = len(dataset) // batch_size
    train_batches = 4 * total_batches // 5
    train, test = dataset[:train_batches*batch_size], dataset[train_batches*batch_size:]
    sess = tf.InteractiveSession()
    sess.run(initialize_all)
    print("Starting training!")
    for epoch in range(300):
        for batch in range(train_batches):
            data = train[batch*batch_size:(batch+1)*batch_size]
            reviews = [sample['x'] for sample in data]
            labels  = [sample['y'] for sample in data]
            labels = np.array(labels).reshape([-1, 4])
            _, l, acc = sess.run([train_step, loss, accuracy], feed_dict={x: reviews, y: labels})
        if epoch % 50 == 0:
            print("Epoch", epoch, "Loss", l, "Acc", acc)
        random.shuffle(train)

    # Evaluate on test set
    test_reviews = [sample['x'] for sample in test]
    test_labels  = [sample['y'] for sample in test]
    test_labels = np.array(test_labels).reshape([-1, 4])
    acc = sess.run(accuracy, feed_dict={x: test_reviews, y: test_labels})
    print("Evaluation accuracy:", acc)
    sess.close()
