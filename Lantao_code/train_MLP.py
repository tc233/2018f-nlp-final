import pandas as pd
import sys
# import nltk
import seaborn as sns
import matplotlib.pyplot as plt

from urllib.request import urlretrieve
import os
import numpy as np
import h5py
import string
import tensorflow as tf


cwd = sys.path[0]  # real cwd
df = pd.read_csv(cwd + '/../dataset/lyrics_clean.csv')


labels = {'Pop': [1.,0.,0.,0.,0.,0.,0.,0.,0.,0.], 'Rock': [0.,1.,0.,0.,0.,0.,0.,0.,0.,0.],
    'Hip-Hop': [0.,0.,1.,0.,0.,0.,0.,0.,0.,0.], 'Metal': [0.,0.,0.,1.,0.,0.,0.,0.,0.,0.],
    'Country': [0.,0.,0.,0.,1.,0.,0.,0.,0.,0.], 'Jazz': [0.,0.,0.,0.,0.,1.,0.,0.,0.,0.],
    'Electronic': [0.,0.,0.,0.,0.,0.,1.,0.,0.,0.], 'R&B': [0.,0.,0.,0.,0.,0.,0.,1.,0.,0.],
    'Indie': [0.,0.,0.,0.,0.,0.,0.,0.,1.,0.], 'Folk': [0.,0.,0.,0.,0.,0.,0.,0.,0.,1.]}

tf.reset_default_graph()
# Placeholders for input
X = tf.placeholder(tf.float32, [None, 300])
y = tf.placeholder(tf.float32, [None, 10])
# Three-layer MLP
h1 = tf.layers.dense(X, 196, tf.nn.relu)
h2 = tf.layers.dense(h1, 48, tf.nn.relu)
logits = tf.layers.dense(h2, 10)
probabilities = tf.sigmoid(logits)
# Loss and metrics
loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=y))
correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(logits, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
initialize_all = tf.global_variables_initializer()
train_step = tf.train.GradientDescentOptimizer(0.05).minimize(loss)


def convert_line(genre, lyrics):
    remove_punct=str.maketrans('','',string.punctuation)
    words = lyrics.translate(remove_punct).lower().split()

    embeddings = [normalized_embeddings[index[w]] for w in words
                  if w in index]
    x = np.mean(np.vstack(embeddings), axis=0)
    y = labels[genre]
    return {'x': x, 'y': y}


if __name__ == '__main__':

    if not os.path.isfile('mini.h5'):
        print("Downloading Conceptnet Numberbatch word embeddings...")
        conceptnet_url = 'http://conceptnet.s3.amazonaws.com/precomputed-data/2016/numberbatch/17.06/mini.h5'
        urlretrieve(conceptnet_url, 'mini.h5')

    with h5py.File('mini.h5', 'r') as f:
        all_words = [word.decode('utf-8') for word in f['mat']['axis1'][:]]
        all_embeddings = f['mat']['block0_values'][:]
    english_words = [word[6:] for word in all_words if word.startswith('/c/en/')]
    english_word_indices = [i for i, word in enumerate(all_words) if word.startswith('/c/en/')]
    english_embedddings = all_embeddings[english_word_indices]
    norms = np.linalg.norm(english_embedddings, axis=1)
    normalized_embeddings = english_embedddings.astype('float32') / norms.astype('float32').reshape([-1, 1])
    index = {word: i for i, word in enumerate(english_words)}

    #labels_list = ['Pop', 'Rock', 'Hip-Hop', 'Metal', 'Country', 'Jazz', 'Electronic', 'R&B', 'Indie', 'Folk']
    labels_list = ['Pop', 'R&B']
    df_ans = df[df['genre'] == 'Pop'][0:1000]
    #df_ans = df[df['genre'] == 'Hip-Hop'][0:1000]
    for curr_label in labels_list[1:]:
        curr_df = df[df['genre'] == curr_label][0:1000]
        df_ans = pd.concat([df_ans, curr_df], axis=0, ignore_index=True)

    genres = df_ans['genre']
    lyrics = df_ans['lyrics']
    print(len(genres))
    #convert_line(genres[0], lyrics[0])

    dataset = [convert_line(genres[i], lyrics[i]) for i in range(len(genres))]

    import random
    random.shuffle(dataset)
    batch_size = 100
    total_batches = len(dataset) // batch_size
    train_batches = 4 * total_batches // 5
    train, test = dataset[:train_batches*batch_size], dataset[train_batches*batch_size:]
    sess = tf.InteractiveSession()
    sess.run(initialize_all)
    print("Starting training!")
    for epoch in range(1700):
        for batch in range(train_batches):
            data = train[batch*batch_size:(batch+1)*batch_size]
            reviews = [sample['x'] for sample in data]
            labels  = [sample['y'] for sample in data]
            labels = np.array(labels).reshape([-1, 10])
            _, l, acc = sess.run([train_step, loss, accuracy], feed_dict={X: reviews, y: labels})
        if epoch % 50 == 0:
            print("Epoch", epoch, "Loss", l, "Acc", acc)
        random.shuffle(train)

    # Evaluate on test set
    test_reviews = [sample['x'] for sample in test]
    test_labels  = [sample['y'] for sample in test]
    test_labels = np.array(test_labels).reshape([-1, 10])
    acc = sess.run(accuracy, feed_dict={X: test_reviews, y: test_labels})
    print("Final accuracy:", acc)
    sess.close()
