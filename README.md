# 2018f-nlp-final

2018 fall NLP Final Project

**🌟🌟🌟[Click to see the final report](/nlp-final-report.pdf)🌟🌟🌟**

**⭐⭐[Also, check our the addition note if you like!](/addition-notes.pdf)⭐⭐**

## Group member:

Xiaoyang Liu (xl149@duke.edu)

Ting Chen (tc233@duke.edu)

Lantao Li (ll285@duke.edu)

---

## Code usage instruction

More update will go to: https://gitlab.oit.duke.edu/tc233/2018f-nlp-final

## Dataset acquisition

The original dataset can be found at: https://www.kaggle.com/gyani95/380000-lyrics-from-metrolyrics

Since the original dataset is big, we compress and upload it to my GitLab repo. You can find it under [/dataset/lyrics_original.zip](/dataset/lyrics_original.zip)

Also, we created a cleaned version for further use. You can find it under [/dataset/lyrics_clean.zip](/dataset/lyrics_original.zip)

All the analysis and classification are done with the cleaned dataset. If you don't want to use our provided one, you could also generate the cleaned dataset with [/Ting_code/step0_clean_csv.py](/Ting_code/step0_clean_csv.py) from original dataset. Just make sure its path is correct.

- Before running this script, make sure you have downloaded the lyrics_original.zip above and decompressed it to dataset folder.

### For Lantao’s code

- Pre-trained Word embedding to download

conceptnet 17.06 mini.h5, can be found on https://github.com/commonsense/conceptnet-numberbatch,

or use the download link inside my python script: http://conceptnet.s3.amazonaws.com/precomputed-data/2016/numberbatch/17.06/mini.h5

- GloVe.6B

which can be found on https://nlp.stanford.edu/projects/glove/. And it needs to be in the same directory with the code

Dataset used in the code should also be within the /dataset directory, including lyrics_clean.csv and lyrics_clean_small_1500.csv 

### For Ting’s code

Dataset should be within /dataset directory.

#### File structure

- step0_best_artist_in_every_genre.py

generate a series of files into /dataset/rankings folder, to preview the artist with most lyrics under each genre

- step0_clean_csv.py

using several rules to clean the original data

- step0_clean_small_testset_artist.py

given a list of artists and a size of lyrics, generate a table contains artists, each with N songs.

- step0_clean_small_testset_genres.py

given a list of genres and a size of lyrics, generate a table contains only listed genres, each with N songs.

- step0_clean_small_testset_year.py

given a list of decades and a size of lyrics, generate a table contains years, each with N songs.

	- Note: year classification is obsolete.

- step0_metrics.py

load a csv table and provide some basic information about it. 

- step0_preview_dataset.py

load a csv table and preview its first 50 lines. for debug use.

- step1_chunking.py

test genre classification accuracy with word count method. accuracy ratio will be in terminal output.

- step1_entropy.py

test genre classification accuracy with entropy method. accuracy ratio will be in terminal output.

- step1_POS.py

test genre classification accuracy with PoS tagging method. accuracy ratio will be in terminal output.

	- note: this requires longer time to run

- step1_sentiment.py

test genre classification accuracy with sentiment analysis method. accuracy ratio will be in terminal output.

- step1_trigram_hitrate.py

test genre classification accuracy with ngram(bigram, trigram) method. accuracy ratio will be in terminal output.

	- note: you can select either using bigram or trigram at the head of the script.

- step2_chunking.py

test artist classification accuracy with word count method. accuracy ratio will be in terminal output.

- step2_entropy.py

test artist classification accuracy with entropy method. accuracy ratio will be in terminal output.

- step2_POS.py

test artist classification accuracy with PoS tagging method. accuracy ratio will be in terminal output.

- step2_sentiment.py

test artist classification accuracy with sentiment analysis method. accuracy ratio will be in terminal output.

- step2_trigram_hitrate.py

test artist classification accuracy with ngram(bigram, trigram) method. accuracy ratio will be in terminal output.

	- note: you can select either using bigram or trigram at the head of the script.

- step4_clustering.py

cluster given artists and plot an image.

### For Xiaoyang’s code

Required packages need to be installed.

## Required Packages

- tensorflow, pandas, h5py, tqdm, nltk, matplotlib, pytorch, tqdm

- Seaborn, matplotlib

- Any other packages that we missed to mention or needed when running the script. (next time we might use venv and pip freeze requirement...)

---

# Below are some other discussions when we devised this project.

## Preliminary Ideas

Text generation using GAN

- Easy to evaluate, multiple algorithms can be implemented
https://github.com/geek-ai/Texygen

- Text classification

Visualize word cloud or with graph

文本分类，反馈自我改进的能力

Information theory, entropy, word count, chunking

predict genre from title

## Discussion record(In Chinese)

### 预处理：
洗掉非英文歌曲步骤：去除非英文词出现的歌曲：如果歌词前30词概率低于80%，删除该歌
洗掉所有genre为other和Not Available的

传统：N-gram，信息熵，转移熵，chunking平均每个句子长度for每首歌
NLP：sentiment analysis
神经网络：他们做

### 想做的内容：
分类作者
分类风格
分类年份

比较各种算法在各个分类情况下的优劣

根据音乐生成歌词

输入genre生成歌词

图形化Wordcloud，二维三维网络坐标每一首歌，聚类

### 需要确定：
选多少风格，每个风格选多少训练集，测试集，
选多少作者，每个作者选多少训练集，测试集

10个分类，每个分类500首歌，测试随机选50个
20个数量Decending order作者，每个作者100首歌，测试随机每人选10首

---

## Progress temp

header
index,song,year,artist,genre,lyrics

whether to preserve \n or not

随机选和选同一歌手大量的

POS tagger每一个歌词给一个定长的向量记录对应的tag，然后分别比较测试集的平均值以及和其中所有的最小差距，看哪个预测正确更多。

interesting application: classify rap styles.

tri-gram classifier 通过看trigram词组命中率判断类别
统计每一个genre的trigram，然后test set统计trigram看在哪个genre中命中最多


dr-dre
flo-rida
eminem

	种类平均单词长度
	种类平均熵

	artist平均单词长度（violin plot）
	artist情感色块图
	artist平均熵

种类聚类
	artist歌词聚类
	
	trigram频次描述

	注意下标倾斜
    try:
        import matplotlib.pyplot as plt
        plt.imshow(correlations)
        plt.xlabel('Testing set')
        plt.ylabel('Training set')
        plt.title('Training-Testing set pairs with Bigram Algorithm')
        plt.xticks(ticks=range(len(categories)), labels=[i for i in categories])
        plt.xticks(rotation=45)
        plt.yticks(ticks=range(len(categories)), labels=[i for i in categories])
        plt.colorbar()
        plt.show()
    except:
        print("matplotlib may not be correctly set up.")