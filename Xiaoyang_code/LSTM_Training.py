from __future__ import unicode_literals, print_function, division
from io import open
import glob
import os

import unicodedata
import string
import torch
import random

#import helpers
#import cv2
from PIL import Image
import numpy as np # linear algebra
import pandas as pd # data processing,CSV file I/O (e.g. pd.read_csv)
import matplotlib as mpl
import matplotlib.pyplot as plt
from nltk.corpus import stopwords
from scipy.misc import imread
from sklearn.feature_extraction.stop_words import ENGLISH_STOP_WORDS
from torch.autograd import Variable


def findFiles(path): return glob.glob(path)

#print(findFiles('data/names/*.txt'))

all_letters = string.ascii_letters + " .,;'"
n_letters = len(all_letters)


def unicodeToAscii(s):
    return ''.join(
        c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn'
        and c in all_letters
    )

T=pd.read_csv("/../dataset/lyrics_clean_small_1500.csv")

lyrics=""
category_lines = {}
all_categories = []
all_letters = string.ascii_letters + " .,;'-[]\"\"?!:\r\n"
n_letters = len(all_letters) # Plus EOS marker
size=0
for ind,val in T.iterrows():
    genre=val["genre"]
    
    if genre not in all_categories:
        all_categories.append(genre)
        category_lines[genre]=[]
    if len(category_lines[genre])<=100:
        category_lines[genre] += [size] # so category_lines map "genre" to a list of index of the songs belong to that genre
        
    size = size + 1;
        
n_categories = len(all_categories)
# Read a file and split into lines
def readLines(filename):
    lines = open(filename, encoding='utf-8').read().strip().split('\n')
    return [unicodeToAscii(line) for line in lines]

for filename in findFiles('data/names/*.txt'):
    category = os.path.splitext(os.path.basename(filename))[0]
    all_categories.append(category)
    lines = readLines(filename)
    category_lines[category] = lines

n_categories = len(all_categories)


import torch

# Find letter index from all_letters, e.g. "a" = 0
def letterToIndex(letter):
    return all_letters.find(letter)

# Just for demonstration, turn a letter into a <1 x n_letters> Tensor
def letterToTensor(letter):
    tensor = torch.zeros(1, n_letters)
    tensor[0][letterToIndex(letter)] = 1
    return tensor

# Turn a line into a <line_length x 1 x n_letters>,
# or an array of one-hot letter vectors
def lineToTensor(line):
    tensor = torch.zeros(len(line), 1, n_letters)
    for li, letter in enumerate(line):
        tensor[li][0][letterToIndex(letter)] = 1
    return tensor

import torch.nn as nn

class RNN(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(RNN, self).__init__()

        self.hidden_size = hidden_size

        self.i2h = nn.Linear(input_size + hidden_size, hidden_size)
        self.i2h1 = nn.Linear(hidden_size, hidden_size)
        self.i2h2 = nn.Linear(hidden_size, hidden_size)
        self.i2o = nn.Linear(input_size + hidden_size, output_size)
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, input, hidden):
        combined = torch.cat((input, hidden), 1)
        hidden = self.i2h(combined)
        hidden1 = self.i2h1(hidden)
        hidden2 = self.i2h2(hidden1)
        combined2 = torch.cat((input, hidden2), 1)
        output = self.i2o(combined2)
        output = self.softmax(output)
        return output, hidden

    def initHidden(self):
        return torch.zeros(1, self.hidden_size)

class RNN1(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(RNN1, self).__init__()
        self.hidden_size = hidden_size

        self.i2h0 = nn.LSTMCell(input_size+hidden_size, hidden_size)#nn.Linear(n_categories + input_size + hidden_size, hidden_size)##
        self.i2h1 = nn.LSTMCell(input_size+hidden_size, hidden_size)
        self.i2h2 = nn.LSTMCell(input_size+hidden_size, hidden_size)
        self.i2o = nn.Linear(input_size + hidden_size, output_size)#nn.LSTM(n_categories + input_size + hidden_size, output_size, 2)#
        self.o2o = nn.Linear(hidden_size*3+output_size, output_size)#nn.Linear(hidden_size + output_size, output_size)
        self.dropout = nn.Dropout(0.1)
        self.softmax = nn.LogSoftmax()

    def forward(self, input, hidden, cellstate): #category,
        nhidden=hidden.clone()
        ncellstate=cellstate.clone()

        input0 = torch.cat((input, hidden[0]), 1)
        nhidden[0], ncellstate[0] = self.i2h0(input0, (hidden[0], cellstate[0]))

        input1 = torch.cat((input, nhidden[0]), 1)
        nhidden[1], ncellstate[1] = self.i2h1(input1, (hidden[1], cellstate[1]))

        input2 = torch.cat((input, nhidden[1]), 1)
        nhidden[2], ncellstate[2] = self.i2h2(input2, (hidden[2], cellstate[2]))

        output = self.i2o(input0)
        output_combined = torch.cat((nhidden[0], nhidden[1], nhidden[2], output), 1)
        output1 = self.o2o(output_combined)#self.o2o(output_combined)
        output2 = self.dropout(output1)
        output3 = self.softmax(output2)
        return output3, nhidden, ncellstate

    def initHidden(self):
        return Variable(torch.zeros(3, 1, self.hidden_size)) #num of
    
n_hidden = 128
rnn = RNN1(n_letters, n_hidden, n_categories)
rnn.load_state_dict(torch.load('lstmclassifier_smallset_run2-40k')) #remember to update the weight name next run

def categoryFromOutput(output):
    top_n, top_i = output.topk(1)
    category_i = top_i[0].item()
    return all_categories[category_i], category_i


def randomChoice(l):
    return l[random.randint(0, len(l) - 1)]

def randomTrainingExample():
    category = randomChoice(all_categories)
    lineidx = randomChoice(category_lines[category])
    line = T["lyrics"][lineidx]
    #line = randomChoice(category_lines[category])
    category_tensor = torch.tensor([all_categories.index(category)], dtype=torch.long)
    line_tensor = lineToTensor(line)
    return category, line, category_tensor, line_tensor

for i in range(10):
    category, line, category_tensor, line_tensor = randomTrainingExample()
    print('category =', category, '/ line =', line[:10])

criterion = nn.NLLLoss()

learning_rate = 0.005 #0.005 

def train(category_tensor, line_tensor):
    hidden = rnn.initHidden()
    cellstate = rnn.initHidden()

    rnn.zero_grad()

    for i in range(line_tensor.size()[0]):
        output, hidden, cellstate = rnn(line_tensor[i], hidden, cellstate)
        if i>300:
            break;

    loss = criterion(output, category_tensor)
    loss.backward()

    # Add parameters' gradients to their values, multiplied by learning rate
    for p in rnn.parameters():
        p.data.add_(-learning_rate, p.grad.data)

    return output, loss.item()


import time
import math

n_iters = 30000
print_every = 100#5000
plot_every = 100#1000



current_loss = 0
all_losses = []

def timeSince(since):
    now = time.time()
    s = now - since
    m = math.floor(s / 60)
    s -= m * 60
    return '%dm %ds' % (m, s)

start = time.time()

for iter in range(1, n_iters + 1):
    category, line, category_tensor, line_tensor = randomTrainingExample()
    output, loss = train(category_tensor, line_tensor)
    current_loss += loss

    # Print iter number, loss, name and guess
    if iter % print_every == 0:
        guess, guess_i = categoryFromOutput(output)
        correct = '✓' if guess == category else '✗ (%s)' % category
        print('%d %d%% (%s) %.4f %s / %s %s' % (iter, iter / n_iters * 100, timeSince(start), loss, line[0], guess, correct))

    # Add current loss avg to list of losses
    if iter % plot_every == 0:
        all_losses.append(current_loss / plot_every)
        current_loss = 0

torch.save(rnn.state_dict(),'lstmclassifier_smallset_run2-70k')
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

plt.figure()
plt.plot(all_losses)


